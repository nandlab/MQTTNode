#ifndef MQTTNODE_HPP
#define MQTTNODE_HPP

#include <string>
#include <boost/asio/io_context.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include "mqtt_client_cpp.hpp"

class MQTTNode
{
protected:
    /**
     * @brief connection_established_handler
     * Procedure to be called, when the the connection
     * to the MQTT broker is established
     */
    virtual void connection_established_handler();

    /**
     * @brief Interpret received MQTT message
     * @param topic - MQTT message topic
     * @param content - MQTT message content
     * @return true if interpreted, false if message unknown
     */
    virtual bool interpret_msg(const mqtt::buffer &topic, const mqtt::buffer &content);

    typedef std::shared_ptr<
        mqtt::callable_overlay<
            mqtt::async_client<
                mqtt::tcp_endpoint<
                    mqtt::as::ip::tcp::socket,
                    mqtt::strand
                >
            >
        >
    >
    MQTTAsyncClient_t;
    boost::asio::io_context &ioc;
    MQTTAsyncClient_t mqtt_client;
    const std::string node_name;

    const std::string full_topic_signal;

    /**
     * @brief Prepend "<node_name>/" to the topic
     * @param topic
     * @return full topic path
     */
    std::string full_topic(const std::string &topic);

private:
    static const std::string topic_status;
    static const std::string topic_signal;
    static const std::string msg_connect;
    static const std::string status_conn;
    static const std::string status_disconn_clean;
    static const std::string status_disconn_abnorm;
    static const std::string signal_kill;
    static const std::chrono::seconds reconnect_delay;
    const std::string full_topic_status;
    boost::asio::steady_timer reconnect_timer;
    boost::asio::signal_set system_signals;

    bool connack_handler(bool session_present, mqtt::connect_return_code connack_return_code);
    void error_handler(const mqtt::error_code &e);
    void close_handler();
    bool suback_handler(std::uint16_t packet_id,
                        const std::vector<mqtt::suback_return_code> &qoss);
    bool publish_handler(mqtt::optional<std::uint16_t> packet_id,
                         mqtt::publish_options pubopts,
                         const mqtt::buffer &topic_name,
                         const mqtt::buffer &contents);
    bool async_connect_handler(const boost::system::error_code &ec);
    void start_reconnect_timer();
    void clean_disconnect();
    void system_signal_handler(const boost::system::error_code &ec, int signal);

public:
    MQTTNode(boost::asio::io_context &ioc,
             const std::string &broker_hostname,
             std::uint16_t broker_port,
             const std::string &node_name);
    virtual void start();
};

#endif // MQTTNODE_HPP
