#include "MQTTNode.hpp"
#include <boost/bind/bind.hpp>

using namespace boost::placeholders;

const std::string MQTTNode::topic_status ("status");
const std::string MQTTNode::topic_signal ("signal");
const std::string MQTTNode::status_conn           ("CONNECTED");
const std::string MQTTNode::status_disconn_clean  ("DISCONNECTED");
const std::string MQTTNode::status_disconn_abnorm ("DISCONNECTED ABNORMALLY");
const std::string MQTTNode::signal_kill  ("KILL");
const std::chrono::seconds MQTTNode::reconnect_delay (10);

MQTTNode::MQTTNode(boost::asio::io_context &ioc,
                   const std::string &broker_hostname,
                   std::uint16_t broker_port,
                   const std::string &node_name)
    : ioc(ioc), mqtt_client(mqtt::make_async_client(ioc, broker_hostname, broker_port)),
      node_name(node_name), full_topic_status(full_topic(topic_status)), full_topic_signal(full_topic(topic_signal)),
      reconnect_timer(ioc), system_signals(ioc, SIGINT, SIGTERM, SIGHUP)
{
    mqtt_client->set_client_id(node_name);
    mqtt_client->set_will(mqtt::will(mqtt::buffer(boost::string_view(full_topic_status)), mqtt::buffer(boost::string_view(status_disconn_abnorm)), mqtt::publish_options(mqtt::retain::yes)));
    mqtt_client->set_keep_alive_sec(30);
    mqtt_client->set_connack_handler(boost::bind(&MQTTNode::connack_handler, this, _1, _2));
    mqtt_client->set_error_handler(boost::bind(&MQTTNode::error_handler, this, boost::asio::placeholders::error));
    mqtt_client->set_close_handler(boost::bind(&MQTTNode::close_handler, this));
    mqtt_client->set_suback_handler(boost::bind(&MQTTNode::suback_handler, this, _1, _2));
    mqtt_client->set_publish_handler(boost::bind(&MQTTNode::publish_handler, this, _1, _2, _3, _4));
}

bool MQTTNode::async_connect_handler(const boost::system::error_code &ec) {
    std::cout << "Async connect handler called, error code: " << ec.message() << std::endl;
    if (ec) { start_reconnect_timer(); }
    return true;
}

void MQTTNode::start_reconnect_timer() {
    std::cout << "Reconnecting in " << reconnect_delay.count() << "s..." << std::endl;
    reconnect_timer.expires_after(reconnect_delay);
    reconnect_timer.async_wait([&] (boost::system::error_code const& ec) {
        if (!ec && !mqtt_client->connected()) {
            std::cout << "Trying to connect again..." << std::endl;
            mqtt_client->async_connect(boost::bind(&MQTTNode::async_connect_handler, this, _1));
        }
    });
}

std::string MQTTNode::full_topic(const std::string &topic) {
    return node_name + "/" + topic;
}

void MQTTNode::connection_established_handler() {
    reconnect_timer.cancel();
    mqtt_client->async_publish(full_topic_status, status_conn, mqtt::retain::yes);
    mqtt_client->async_subscribe(full_topic_signal, mqtt::qos::at_most_once);
}

bool MQTTNode::connack_handler(bool session_present, mqtt::connect_return_code connack_return_code) {
    std::cout << "Connack handler called" << std::endl;
    std::cout << "Session Present: " << std::boolalpha << session_present << std::endl;
    std::cout << "Connack Return Code: " << connack_return_code << std::endl;
    if (connack_return_code == mqtt::connect_return_code::accepted) {
        connection_established_handler();
    }
    else {
        start_reconnect_timer();
    }
    return true;
}

void MQTTNode::error_handler(const mqtt::error_code &ec) {
    std::cout << "Connection failed: " << ec << "." << std::endl;
    start_reconnect_timer();
}

void MQTTNode::close_handler() {
    std::cout << "Connection closed." << std::endl;
    ioc.stop();
}

bool MQTTNode::suback_handler(std::uint16_t packet_id,
                    const std::vector<mqtt::suback_return_code> &qoss)
{
    std::cout << "suback received. packet_id: " << packet_id << std::endl;
    for (mqtt::suback_return_code const& e : qoss) {
        std::cout << "subscribe result: " << e << std::endl;
    }
    return true;
}

void MQTTNode::clean_disconnect() {
    reconnect_timer.cancel();
    mqtt_client->async_publish(full_topic_status, status_disconn_clean, mqtt::retain::yes);
    mqtt_client->async_disconnect();
}

bool MQTTNode::publish_handler
(
        mqtt::optional<std::uint16_t> packet_id,
        mqtt::publish_options pubopts,
        const mqtt::buffer &topic_name,
        const mqtt::buffer &content
) {
    std::cout << "publish received."
              << " dup: "    << pubopts.get_dup()
              << " qos: "    << pubopts.get_qos()
              << " retain: " << pubopts.get_retain() << std::endl;
    if (packet_id)
        std::cout << "packet_id: " << *packet_id << std::endl;
    std::cout << "topic_name: " << topic_name << std::endl;
    std::cout << "contents: " << content << std::endl;
    interpret_msg(topic_name, content);
    return true;
}

void MQTTNode::system_signal_handler(const boost::system::error_code &ec, int signal) {
    if (!ec) {
        std::cout << "Signal " << signal << " received." << std::endl;
    }
    else {
        std::cout << "Boost error while waiting for signal: " << ec.message() << std::endl;
    }
    if (mqtt_client->connected()) {
        std::cout << "Disconnecting..." << std::endl;
        clean_disconnect();
    }
    else {
        ioc.stop();
    }
}

void MQTTNode::start() {
    system_signals.async_wait(boost::bind(&MQTTNode::system_signal_handler, this, _1, _2));
    mqtt_client->async_connect(boost::bind(&MQTTNode::async_connect_handler, this, _1));
}

bool MQTTNode::interpret_msg(const mqtt::buffer &topic, const mqtt::buffer &content) {
    if (topic == full_topic_signal) {
        if (content == signal_kill) {
            std::cout << "MQTT kill signal received, exiting." << std::endl;
            clean_disconnect();
        }
    }
    return true;
}
